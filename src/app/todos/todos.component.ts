import { Component, OnInit } from '@angular/core';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';
import { AuthService} from '../auth.service';
import { TodosService} from '../todos.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos = [];

  text;
  newTask;

  showText($event){
    this.text=$event;
    
  }


  constructor(private authService:AuthService, private todosService:TodosService, private db:AngularFireDatabase) { }

  addTask(){
    this.todosService.addTask(this.newTask);
    this.newTask='';
  }

  ngOnInit() {
    this.authService.user.subscribe(user=> {
      this.db.list('/users/'+user.uid+'/todos/').snapshotChanges().subscribe(
        todos=>{
          this.todos = [];
          todos.forEach(
            todo =>{
              let y = todo.payload.toJSON();
               y["$key"] = todo.key;
              this.todos.push(y);
            }
          )
        }
      )

    })


  }

}

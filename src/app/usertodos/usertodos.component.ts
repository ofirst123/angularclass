import { Component, OnInit } from '@angular/core';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';

@Component({
  selector: 'app-usertodos',
  templateUrl: './usertodos.component.html',
  styleUrls: ['./usertodos.component.css']
})
export class UsertodosComponent implements OnInit {

  user = "jack";
  todos = [];
  text;

  showText($event){
    this.text=$event;
    
  }

  constructor(private db:AngularFireDatabase) { }

  changeUser() {
    this.db.list('/users/'+this.user+'/todos/').snapshotChanges().subscribe(
      todos=>{
        this.todos = [];
        todos.forEach(
          todo =>{
            let y = todo.payload.toJSON();
            y["$key"] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )
  }


  ngOnInit() {
    this.db.list('/users/'+this.user+'/todos/').snapshotChanges().subscribe(
      todos=>{
        this.todos = [];
        todos.forEach(
          todo =>{
            let y = todo.payload.toJSON();
            y["$key"] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )
  }

}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Router} from '@angular/router';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';
import { AuthService} from '../auth.service';
import { TodosService} from '../todos.service';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClick=new EventEmitter<any>();
  

  text;
  id;
  status;
  key;
  tempText;

  showButton = false;
  showEditField=false;

  delete(){
    this.todosService.delete(this.key);
  }
  showEdit(){
    this.showEditField= true;
    this.tempText=this.text;
   
  }
  save(){
    this.todosService.save(this.key,this.text);
    this.showEditField=false;
  }
  cancel(){
    this.text=this.tempText;
    this.showEditField=false;
  }

  send(){
    console.log(this.text)
    this.myButtonClick.emit(this.text);
  }
  over(){
    this.showButton = true;
  }
  notOver(){
    this.showButton = false;
  }
  constructor(private authService:AuthService, private todosService:TodosService, private db:AngularFireDatabase) { }

  ngOnInit() {
    this.text = this.data.text;
    this.key = this.data.$key;
    
  
  }

}

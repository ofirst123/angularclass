import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AuthService} from '../auth.service';


@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
 errM;

  toRegister(){
    this.router.navigate(['/register']);
  }
  toTodos(){
    this.router.navigate(['/todos']);
  }
  toLogin(){
    this.router.navigate(['/login']);
  }
  toCodes(){
    this.router.navigate(['/codes']);
  }
  toUsertodos(){
    this.router.navigate(['/usertodos']);
  }
  toLogout(){
    this.authService.logout().
      then(value=>{this.router.navigate(['/login'])
    })
    .catch(err=>{
      this.errM = err;
      console.log("blalalalalal");
    })
  }
  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

}

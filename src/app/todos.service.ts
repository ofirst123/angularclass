import { Injectable } from '@angular/core';
import { AuthService} from './auth.service';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class TodosService {


  addTask(text:string){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/todos/').push({'text':text});
    })
  }

  delete(key){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/todos/').remove(key);
    })
  }

  save(key,text:string){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/todos/').update(key,{'text':text});
    })
  }

  constructor(private authService:AuthService, private db:AngularFireDatabase) { }
}
